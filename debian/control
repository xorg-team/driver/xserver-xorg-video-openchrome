Source: xserver-xorg-video-openchrome
Section: x11
Priority: optional
Maintainer: Debian X Strike Force <debian-x@lists.debian.org>
Uploaders: Dylan Aïssi <daissi@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 libdrm-dev [!hurd-any],
 libgl1-mesa-dev | libgl1-dev,
 libx11-dev,
 libxvmc-dev,
 pkg-config,
 quilt,
 x11proto-dev,
 xserver-xorg-dev (>= 2:21.1),
 xutils-dev
Build-Conflicts:
 autoconf2.13
Standards-Version: 4.1.1
Homepage: https://www.freedesktop.org/wiki/Openchrome/
Vcs-Git: https://salsa.debian.org/xorg-team/driver/xserver-xorg-video-openchrome.git
Vcs-Browser: https://salsa.debian.org/xorg-team/driver/xserver-xorg-video-openchrome

Package: xserver-xorg-video-openchrome
Architecture: amd64 hurd-i386 i386 kfreebsd-amd64 kfreebsd-i386 x32
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
 ${xviddriver:Depends}
Provides:
 ${xviddriver:Provides}
Description: X.Org X server -- OpenChrome display driver
 This package provides the 'openchrome' driver for the VIA Technologies
 UniChrome and Chrome9 IGPs chipsets. The following chips should be
 supported: CLE266, KM400(A), KN400(A), P4M800, K8M800, K8N800, PM800,
 PN800, PM880, CN333, CN400, P4M800 Pro, VN800, CN700, CX700, VX700,
 P4M890, VN890, CN800, K8M890, K8N890, P4M900, VN896, CN896, VX800, VX820,
 VX855, VX875, VX900.
 .
 This package is built from the FreeDesktop.org xf86-video-openchrome driver.

Package: openchrome-tool
Architecture: amd64 i386 kfreebsd-amd64 kfreebsd-i386 x32
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
 xserver-xorg-video-openchrome (= ${binary:Version})
Description: Tool for debugging the OpenChrome display driver
 via_regs_dump is a registers dumper tool for debugging the OpenChrome driver.
 This tool can read and write registers and display some others information
 useful for debugging the driver.
